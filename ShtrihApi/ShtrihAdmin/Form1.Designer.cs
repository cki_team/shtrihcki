﻿namespace ShtrihAdmin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowProperties = new System.Windows.Forms.Button();
            this.btnXReport = new System.Windows.Forms.Button();
            this.btnZReport = new System.Windows.Forms.Button();
            this.lblResult = new System.Windows.Forms.Label();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.kassir = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.AmountNal = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.AmountBn = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.useNal = new System.Windows.Forms.RadioButton();
            this.useBn = new System.Windows.Forms.RadioButton();
            this.qty = new System.Windows.Forms.NumericUpDown();
            this.price = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tax = new System.Windows.Forms.NumericUpDown();
            this.name = new System.Windows.Forms.TextBox();
            this.addLine = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AmountNal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountBn)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tax)).BeginInit();
            this.SuspendLayout();
            // 
            // btnShowProperties
            // 
            this.btnShowProperties.Location = new System.Drawing.Point(12, 99);
            this.btnShowProperties.Name = "btnShowProperties";
            this.btnShowProperties.Size = new System.Drawing.Size(129, 23);
            this.btnShowProperties.TabIndex = 0;
            this.btnShowProperties.Text = "Состояние";
            this.btnShowProperties.UseVisualStyleBackColor = true;
            this.btnShowProperties.Click += new System.EventHandler(this.btnShowProperties_Click);
            // 
            // btnXReport
            // 
            this.btnXReport.Location = new System.Drawing.Point(12, 12);
            this.btnXReport.Name = "btnXReport";
            this.btnXReport.Size = new System.Drawing.Size(129, 23);
            this.btnXReport.TabIndex = 1;
            this.btnXReport.Text = "X Отчет";
            this.btnXReport.UseVisualStyleBackColor = true;
            this.btnXReport.Click += new System.EventHandler(this.btnXReport_Click);
            // 
            // btnZReport
            // 
            this.btnZReport.Location = new System.Drawing.Point(12, 41);
            this.btnZReport.Name = "btnZReport";
            this.btnZReport.Size = new System.Drawing.Size(129, 23);
            this.btnZReport.TabIndex = 2;
            this.btnZReport.Text = "Закрыть смену";
            this.btnZReport.UseVisualStyleBackColor = true;
            this.btnZReport.Click += new System.EventHandler(this.btnZReport_Click);
            // 
            // lblResult
            // 
            this.lblResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(9, 434);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(62, 13);
            this.lblResult.TabIndex = 3;
            this.lblResult.Text = "Результат:";
            // 
            // tbResult
            // 
            this.tbResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbResult.Location = new System.Drawing.Point(12, 450);
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(927, 20);
            this.tbResult.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 70);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Открыть смену";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OpenSession_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(209, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Текущий кассир";
            // 
            // kassir
            // 
            this.kassir.Location = new System.Drawing.Point(298, 14);
            this.kassir.Name = "kassir";
            this.kassir.Size = new System.Drawing.Size(129, 20);
            this.kassir.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(298, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Аннулировать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(298, 41);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(129, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Продолжить печать";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Continue_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(298, 99);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(129, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "Повторить чек";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Repeat_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(6, 135);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(182, 23);
            this.button5.TabIndex = 7;
            this.button5.Text = "Возврат открыт";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 145);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(888, 302);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.addLine);
            this.tabPage1.Controls.Add(this.name);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.tax);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.price);
            this.tabPage1.Controls.Add(this.qty);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.AmountBn);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.AmountNal);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(880, 276);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(192, 74);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // AmountNal
            // 
            this.AmountNal.DecimalPlaces = 2;
            this.AmountNal.Location = new System.Drawing.Point(16, 30);
            this.AmountNal.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.AmountNal.Name = "AmountNal";
            this.AmountNal.Size = new System.Drawing.Size(120, 20);
            this.AmountNal.TabIndex = 8;
            this.AmountNal.ThousandsSeparator = true;
            this.AmountNal.ValueChanged += new System.EventHandler(this.AmountNal_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Сумма наличными";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(161, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Сумма безнал";
            // 
            // AmountBn
            // 
            this.AmountBn.DecimalPlaces = 2;
            this.AmountBn.Location = new System.Drawing.Point(164, 30);
            this.AmountBn.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.AmountBn.Name = "AmountBn";
            this.AmountBn.Size = new System.Drawing.Size(120, 20);
            this.AmountBn.TabIndex = 11;
            this.AmountBn.ThousandsSeparator = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.useBn);
            this.groupBox1.Controls.Add(this.useNal);
            this.groupBox1.Location = new System.Drawing.Point(305, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // useNal
            // 
            this.useNal.AutoSize = true;
            this.useNal.Location = new System.Drawing.Point(6, 19);
            this.useNal.Name = "useNal";
            this.useNal.Size = new System.Drawing.Size(76, 17);
            this.useNal.TabIndex = 0;
            this.useNal.TabStop = true;
            this.useNal.Text = "Наличные";
            this.useNal.UseVisualStyleBackColor = true;
            // 
            // useBn
            // 
            this.useBn.AutoSize = true;
            this.useBn.Location = new System.Drawing.Point(6, 52);
            this.useBn.Name = "useBn";
            this.useBn.Size = new System.Drawing.Size(62, 17);
            this.useBn.TabIndex = 1;
            this.useBn.TabStop = true;
            this.useBn.Text = "Безнал";
            this.useBn.UseVisualStyleBackColor = true;
            // 
            // qty
            // 
            this.qty.Location = new System.Drawing.Point(305, 174);
            this.qty.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.qty.Name = "qty";
            this.qty.Size = new System.Drawing.Size(120, 20);
            this.qty.TabIndex = 13;
            this.qty.ThousandsSeparator = true;
            // 
            // price
            // 
            this.price.DecimalPlaces = 2;
            this.price.Location = new System.Drawing.Point(447, 174);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(120, 20);
            this.price.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(302, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Количество";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(444, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Цена с НДС";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(571, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "НДС";
            // 
            // tax
            // 
            this.tax.Location = new System.Drawing.Point(574, 174);
            this.tax.Name = "tax";
            this.tax.Size = new System.Drawing.Size(120, 20);
            this.tax.TabIndex = 17;
            this.tax.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(6, 174);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(268, 20);
            this.name.TabIndex = 19;
            // 
            // addLine
            // 
            this.addLine.Location = new System.Drawing.Point(700, 174);
            this.addLine.Name = "addLine";
            this.addLine.Size = new System.Drawing.Size(153, 23);
            this.addLine.TabIndex = 20;
            this.addLine.Text = "Добавить";
            this.addLine.UseVisualStyleBackColor = true;
            this.addLine.Click += new System.EventHandler(this.addLine_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(6, 219);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(182, 23);
            this.button6.TabIndex = 21;
            this.button6.Text = "Возврат закрыть";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 482);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.kassir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnZReport);
            this.Controls.Add(this.btnXReport);
            this.Controls.Add(this.btnShowProperties);
            this.Name = "Form1";
            this.Text = "Тест драйвера ФР";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AmountNal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountBn)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tax)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShowProperties;
        private System.Windows.Forms.Button btnXReport;
        private System.Windows.Forms.Button btnZReport;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox kassir;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown AmountNal;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button addLine;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown tax;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown price;
        private System.Windows.Forms.NumericUpDown qty;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton useBn;
        private System.Windows.Forms.RadioButton useNal;
        private System.Windows.Forms.NumericUpDown AmountBn;
        private System.Windows.Forms.Label label3;
    }
}

