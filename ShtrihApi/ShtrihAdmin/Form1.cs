﻿using System;
using System.Windows.Forms;
using ShtrihApi;

namespace ShtrihAdmin
{
    public partial class Form1 : Form
    {
        readonly IShtrikhApi _driver;

        private Check _check;

        private ShtrikhApiFactory factory;
        public Form1()
        {
            InitializeComponent();
            factory = new ShtrikhApiFactory();
            _driver = factory.ApiCreate(debugMode:false,ip: "192.168.5.104",port: 7778);
            kassir.Text = _driver.GetUserName();
        }

        private void UpdateResult(DriverResult xReport)
        {
            tbResult.Text = $@"Результат: {xReport.ResultCode}, {xReport.ResultCodeDescription}";
        }
        private void btnXReport_Click(object sender, EventArgs e)
        {
            UpdateResult(_driver.XReport());
        }

        private void btnZReport_Click(object sender, EventArgs e)
        {
            UpdateResult(_driver.CloseSession());
        }

        private void btnShowProperties_Click(object sender, EventArgs e)
        {
            string status;

            var driver = _driver.GetECRStatus(out status);

            UpdateResult(driver);

            MessageBox.Show(status, @"Технические данные");
        }

        private void OpenSession_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(kassir.Text))
            {
                MessageBox.Show(@"Имя кассира должно быть указано");
                return;
            }

            UpdateResult(_driver.OpenSession(kassir.Text));
        }

        private void Continue_Click(object sender, EventArgs e)
        {
            UpdateResult(_driver.ContinueLastCheck());
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            UpdateResult(_driver.CancelCheck());
        }

        private void Repeat_Click(object sender, EventArgs e)
        {
            UpdateResult(_driver.RepeatLastCheck());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var checkInfo = new CheckInfo();
            checkInfo.ShouldRound = false;
            checkInfo.Email = "";
            checkInfo.CheckType = CheckType.ReturnSale;
            checkInfo.SaleNumber = "";
            checkInfo.UserName = kassir.Text;
            if (useNal.Checked)
            {
                checkInfo.CashValue = AmountNal.Value;
                checkInfo.PaymentType = PaymentType.Cash;
            }
            else
            {
                checkInfo.CreditCardValue = AmountBn.Value;
            }

            _check = factory.CheckCreate(checkInfo);

        }

        private void AmountNal_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void addLine_Click(object sender, EventArgs e)
        {
            var saleInfo = new SaleInfo();
            saleInfo.Quantity = (double) qty.Value;
            saleInfo.Price = price.Value;
            saleInfo.DiscountSum = 0;
            saleInfo.Name = name.Text;
            saleInfo.Tax = (double)tax.Value;
            _check.Add(saleInfo);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            UpdateResult(_driver.SendCheck(_check));
        }
    }
}
