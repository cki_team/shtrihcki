﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShtrihApi;

namespace ShtrihApiTest
{
    [TestClass]
    public class UnitTest1
    {
#if DEBUG
        [TestMethod]
        public void ConsolePrintTest()
        {
            var api = new ShtrikhApiFactory().ApiCreate(debugMode: false, paperWidth: 32);

            api.OpenSession("Петя");
            Check check = new Check(new CheckInfo()
            {
                UserName = "Новиков Дмитрий",
                Email = "l.andreev90@gmail.com",
                CheckType = CheckType.Sale,
                DiscountOnCheck = 10,
                PaymentType = PaymentType.Mixed,
                CreditCardValue = 1000,
                CashValue = 1000,
                ShouldRound = true
            })
            {
                new SaleInfo()
                {
                    Tax = 18,
                    Quantity = 5,
                    Price = (decimal) 123.22,
                    Name = "покупка №1",
                    PaymentTypeSign = PaymentTypeSign.Prepayment100

                },
                new SaleInfo()
                {
                    Tax = 10,
                    Quantity = 2,
                    Price = (decimal) 111.11113,
                    Name = "покупка №2"
                },
                new SaleInfo()
                {
                    Tax = 10,
                    Quantity = 2,
                    Price = (decimal) 111.11113,
                    Name = "покупка №2я пошел гулять туда куда нельзя было, но почему-то я решил это сделать"
                }
            };

            var result = api.SendCheck(check);

            Console.WriteLine(result.ResultCodeDescription);
        }

        [TestMethod]
        public void WaitOneTest()
        {
            Assert.AreEqual(true, WaitOne.DoWork(() => Thread.Sleep(new Random().Next(100, 1000)), 1000));

            Assert.AreEqual(false, WaitOne.DoWork(() => Thread.Sleep(new Random().Next(1000, 2000)), 1000));
        }
        //#else
        [TestMethod]
        public void TestMethod1()
        {
            var api = new ShtrikhApiFactory().ApiCreate(debugMode: false);

            Check check = new Check(new CheckInfo()
            {
                UserName = "Новиков Дмитрий",
                Email = "l.andreev90@gmail.com",
                CheckType = CheckType.Sale
            })
            {
                new SaleInfo()
                {
                    Tax = 18,
                    Quantity = 5,
                    Price = (decimal) 123.22,
                    Name = "покупка №1",
                    PaymentTypeSign = PaymentTypeSign.Prepayment100

                },
                //new SaleInfo()
                //{
                //    Tax = 10,
                //    Quantity = 2,
                //    Price = (decimal) 111.11113,
                //    Name = "покупка №2"
                //},
                //new SaleInfo()
                //{
                //    Tax = 10,
                //    Quantity = 2,
                //    Price = (decimal) 111.11113,
                //    Name = "покупка №2я пошел гулять туда куда нельзя было, но почему-то я решил это сделать"
                //}
            };

            var result = api.SendCheck(check);

            Assert.AreEqual(0, result.ResultCode, result.ResultCodeDescription);
        }

        [TestMethod]
        public void CashTest()
        {
            var api = new ShtrikhApiFactory().ApiCreate();
            api.OpenSession("Петя");
            Check check = new Check(new CheckInfo()
            {
                UserName = "Новиков Дмитрий",
                PaymentType = PaymentType.Cash,
                CashValue = 1000
            })
            {
                new SaleInfo()
                {
                    Tax = 18,
                    Quantity = 5,
                    Price = (decimal) 123.22,
                    Name = "покупка №1"
                },
                new SaleInfo()
                {
                    Tax = 10,
                    Quantity = 2,
                    Price = (decimal) 111.1999999,
                    Name = "покупка №2"
                }
            };

            var result = api.SendCheck(check);

            Assert.AreEqual(0, result.ResultCode, result.ResultCodeDescription);
        }

        [TestMethod]
        public void MixedTest()
        {
            var api = new ShtrikhApiFactory().ApiCreate(debugMode: false, paperWidth: 32);
            api.OpenSession("Петя");
            Check check = new Check(new CheckInfo()
            {
                UserName = "Новиков Дмитрий",
                PaymentType = PaymentType.CreditCard,
                CashValue = 0,
                CreditCardValue = 0
            })
            {
                new SaleInfo()
                {
                    Tax = 18,
                    Quantity = 2000,
                    Price = (decimal) 0.19,
                    Name = "покупка №1",
                    PaymentTypeSign = PaymentTypeSign.Prepayment100
                },

            };

            var result = api.SendCheck(check);

            Assert.AreEqual(0, result.ResultCode, result.ResultCodeDescription);
        }

        [TestMethod]
        public void RNTest()
        {
            using (var api = new ShtrikhApiFactory().CreateWidth32("192.168.0.181", 7778))
            {
                //api.OpenSession("Петя");
                Check check = new Check(new CheckInfo()
                {
                    UserName = "Новиков Дмитрий",
                    PaymentType = PaymentType.CreditCard,
                    CashValue = 0,
                    CreditCardValue = 0
                })
                {
                    new SaleInfo()
                    {
                        Tax = 18,
                        Quantity = 1,
                        Price = (decimal) 550,
                        Name = "покупка №13",
                        PaymentTypeSign = PaymentTypeSign.Prepayment100
                    },

                };

                var result = api.SendCheck(check);

                string status;

                api.GetECRStatus(out status);

                Console.Write(status);

                Assert.AreEqual(0, result.ResultCode, result.ResultCodeDescription);

               
            }
        }

        [TestMethod]
        public void SetActiveTest()
        {
            var api = new ShtrikhApiFactory().CreateWidth32("192.168.0.181", 7778);

            var prop = api.GetProperties();

            api.PrintString("Привет");
        }

        [TestMethod]
        public void Continue()
        {
            var api = new ShtrikhApiFactory().CreateWidth32("192.168.137.111", 7778);

            api.ContinueLastCheck();

            string status;

            api.GetECRStatus(out status);

            Console.Write(status);
        }

        [TestMethod]
        public void CorrectionTest()
        {
            var api = new ShtrikhApiFactory().CreateWidth32("192.168.137.111", 7778);

            var result = api.CorrectionReceipt(new CorrectionInfo()
            {
                ResultAmount = 1200,
                CashAmount = 1000,
                CreditAmount = 200,
                VAT18Amount = 216,
            });

            Assert.AreEqual(0, result.ResultCode, result.ResultCodeDescription);
        }
#endif
    }
}
