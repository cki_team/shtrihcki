namespace ShtrihApi
{
    /// <summary>
    /// ��������� ���������� 
    /// </summary>
    public class DriverResult
    {
        /// <summary>
        /// ��� ������
        /// </summary>
        public int ResultCode { get; }

        /// <summary>
        /// �������� ������
        /// </summary>
        public string ResultCodeDescription { get; }

        /// <summary>
        /// ���
        /// </summary>
        public DriverResultType DriverResultType { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultCode"></param>
        /// <param name="resultCodeDescription"></param>
        /// <param name="driverResultType"></param>
        public DriverResult(int resultCode, string resultCodeDescription, DriverResultType driverResultType)
        {
            ResultCode = resultCode;
            ResultCodeDescription = resultCodeDescription;
            DriverResultType = driverResultType;
        }
    }
}