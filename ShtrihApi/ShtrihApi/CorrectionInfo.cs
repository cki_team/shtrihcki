namespace ShtrihApi
{
    /// <summary>
    /// ���������� �� ���� ���������
    /// </summary>
    public class CorrectionInfo
    {
        /// <summary>
        /// ��� ���� ���������
        /// </summary>
        public CorrectionType CorrectionType { get; set; } = CorrectionType.Self;

        /// <summary>
        /// ������� �������
        /// </summary>
        public CorrectionSign CorrectionSign { get; set; } = CorrectionSign.Out;

        /// <summary>
        /// ����� ����
        /// </summary>
        public decimal ResultAmount { get; set; }

        /// <summary>
        /// ����� �� ���� ���������
        /// </summary>
        public decimal CashAmount { get; set; }

        /// <summary>
        /// ����� �� ���� ������������
        /// </summary>
        public decimal CreditAmount { get; set; }

        /// <summary>
        /// ����� �� ���� �����������
        /// </summary>
        public decimal PrepayAmount { get; set; }

        /// <summary>
        /// ����� �� ���� �����������
        /// </summary>
        public decimal PostpayAmount { get; set; }

        /// <summary>
        /// ����� ��� 18%
        /// </summary>
        public decimal VAT18Amount { get; set; }

        /// <summary>
        /// ����� ��� 10%
        /// </summary>
        public decimal VAT10Amount { get; set; }

        /// <summary>
        /// ����� ��� 0%
        /// </summary>
        public decimal VAT0Amount { get; set; }

        /// <summary>
        /// ����� ��� ���
        /// </summary>
        public decimal NoVATAmount { get; set; }

        /// <summary>
        /// ����� ������� �� ������ 18/118
        /// </summary>
        public decimal VAT18118Amount { get; set; }

        /// <summary>
        /// ����� ������� �� ������ 10/110
        /// </summary>
        public decimal VAT10110Amount { get; set; }

        /// <summary>
        /// ����� ����
        /// </summary>
        public int ChequeNumber { get; set; }

        /// <summary>
        /// ����� ���������� ���������
        /// </summary>
        public int DocumentNumber { get; set; }
    }
}