﻿using System;
using System.Runtime.Serialization;

namespace ShtrihApi
{
    public class ShtrihApiException : Exception
    {
        public ShtrihApiException()
        {
        }

        public ShtrihApiException(string message) : base(message)
        {
        }

        public ShtrihApiException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ShtrihApiException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}