using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ShtrihApi
{
    internal class ConsoleShtrikhApi : IShtrikhApi,IDisposable
    {
        private readonly int _maxStringLength = 32;

        private static bool _isOpenSession;
        private static DateTime _openSessionDateTime;
        private static Check _currentCheck;
        private static string _openSessionUserName;
        private readonly string _address;
        private readonly string _company;

        /// <summary>
        /// ��� ����������
        /// </summary>
        public event EventHandler<CheckPrintedEventArgs> CheckPrinted;

        public ConsoleShtrikhApi(string company, string address, int? widthPrintingTape = null)
        {
            _company = company;
            _address = address;

            if(widthPrintingTape.HasValue)
                _maxStringLength = widthPrintingTape.Value;
        }

        /// <summary>
        /// ������������ ���
        /// </summary>
        /// <returns><see cref="DriverResult"/> - ���������</returns>
        public DriverResult CancelCheck()
        {
            if (_currentCheck == null)
                return new DriverResult(52, "��� �������� ����", DriverResultType.Error);

            _currentCheck = null;

            return new DriverResult(0, "������ ���", DriverResultType.Information);
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        /// <returns><see cref="DriverResult"/> - ���������</returns>
        public DriverResult ContinueLastCheck()
        {
            if (_currentCheck == null)
                return new DriverResult(52, "��� �������� ����", DriverResultType.Error);

            return SendCheck(_currentCheck);
        }

        private string DescriptionAttr<T>(T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }

        /// <summary>
        /// ����������� ���
        /// </summary>
        /// <param name="check">���</param>
        /// <returns><see cref="DriverResult"/> - ���������</returns>
        public DriverResult SendCheck(Check check)
        {
            if (check == null) throw new ArgumentNullException(nameof(check));


            if (string.IsNullOrEmpty(check.Info.UserName))
            {
                return new DriverResult(10, "��� ������� ������ ���� ������.", DriverResultType.Error);
            }

            if (_isOpenSession == false)
            {
                OpenSession(check.Info.UserName);
                //  return new DriverResult(10, "������� ������� �����, ������ ��� ����������.", DriverResultType.Error);
            }


            if (_isOpenSession && _openSessionDateTime > DateTime.Now + TimeSpan.FromHours(24))
            {
                return new DriverResult(10, "������� ������� �����, ������ ��� ����������.", DriverResultType.Error);
            }

            if (_currentCheck != null)
            {
                return new DriverResult(10, "���� �������� ��������. ���������� ������ ��� ����������� ���.", DriverResultType.Error);
            }

            if (check.Info.CashValue < 0)
                return new DriverResult(10, "����� �������� �� ����� ���� �������������.", DriverResultType.Error);

            _currentCheck = check;

            var result = new List<string>();

            result.Add(DescriptionAttr(check.Info.CheckType));

            var company = _company;

            while (string.IsNullOrEmpty(company) == false)
            {
                var str = company.Substring(0, company.Length < _maxStringLength ? company.Length : _maxStringLength);

                result.Add(str);
                company = company.Substring(str.Length);
            }

            var address = _address;

            while (string.IsNullOrEmpty(address) == false)
            {
                var str = address.Substring(0, address.Length < _maxStringLength ? address.Length : _maxStringLength);

                result.Add(str);
                address = address.Substring(str.Length);
            }

            SetUserName(check.Info.UserName);

            result.Add(_openSessionUserName);

            if (check.Info.Email != null)
            {
                result.Add(check.Info.Email);
            }

            decimal sumSale = 0;

            for (var index = 0; index < check.Count(); index++)
            {
                var sale = check.ElementAt(index);

                sumSale += AddSaleToCheck(result, sale, index + 1);
            }

            decimal discount = 0;


            if (check.Info.ShouldRound && sumSale > 1) discount = sumSale % (decimal) check.Info.Round;

            result.Add(new string('=', _maxStringLength));
            result.Add($"����� �����: {sumSale:C2}");
            result.Add($"������: {check.Sum(x=>x.Info.DiscountSum):C2}");
            result.Add($"� ������: {sumSale - discount:C2}");
            


            if (check.Info.PaymentType == PaymentType.Cash)
            {
                if(check.Info.CashValue < sumSale - discount)
                    return new DriverResult(10, "������������ �������", DriverResultType.Error);

                result.Add($"��������: {check.Info.CashValue:C2}");
                result.Add($"�����: {check.Info.CashValue - sumSale + discount:C2}");
            }
            else if (check.Info.PaymentType == PaymentType.CreditCard)
            {
                result.Add($"��������� �����: {sumSale - discount:C2}");
            }
            else
            {
                result.Add($"��������� �����: {check.Info.CreditCardValue:C2}");
                result.Add($"��������: {check.Info.CashValue:C2}");
                result.Add($"�����: {check.Info.CashValue - (sumSale - check.Info.CreditCardValue) + discount:C2}");
            }
            _currentCheck = null;

            CheckPrinted?.Invoke(this, new CheckPrintedEventArgs(result));

            return new DriverResult(0, "������ ���", DriverResultType.Information);
        }

        private decimal AddSaleToCheck(List<string> checkText, Sale sale, int index)
        {
            var price = sale.Info.Price * (decimal)sale.Info.Quantity;
            double tax = sale.Info.TaxObject.Percentage;

            //������

            var taxValue = Math.Round((double)price / (1 + tax / 100) * tax / 100, 2);

            string pointers = new string('.', _maxStringLength);

            var titleString = $"{index}. ";

            var firstName = sale.Info.Name.Length > _maxStringLength - titleString.Length
                ? sale.Info.Name.Substring(0, _maxStringLength - titleString.Length)
                : sale.Info.Name;

            var name = sale.Info.Name.Length > _maxStringLength - titleString.Length
                ? sale.Info.Name.Substring(firstName.Length)
                : null;

            List<string> names = new List<string>
            {
                $"{titleString}{firstName}"
            };

            while (string.IsNullOrEmpty(name) == false)
            {
                var str = name.Substring(0, name.Length < _maxStringLength ? name.Length : _maxStringLength);

                names.Add(str);
                name = name.Substring(str.Length);
            }

            names.ForEach(checkText.Add);

            var priceString = $"{sale.Info.Price:N2}";
            checkText.Add($"����{pointers.Substring(0, _maxStringLength - 4 - priceString.Length)}{priceString}");

            var quantityString = $"{sale.Info.Quantity}";
            checkText.Add($"����������{pointers.Substring(0, _maxStringLength - 10 - quantityString.Length)}{quantityString}");

            var taxString = $"{taxValue:N2}";
            checkText.Add($"��� {tax}%{pointers.Substring(0, _maxStringLength - 7 - taxString.Length)}{taxString}");
            
            var sumString = $"{price:N2}";
            checkText.Add($"���������{pointers.Substring(0, _maxStringLength - 9 - sumString.Length)}{sumString}");

            return price;
        }

        /// <summary>
        /// ������� �����
        /// </summary>
        /// <returns><see cref="DriverResult"/> - ���������</returns>
        public DriverResult OpenSession(string userName)
        {
            if (_isOpenSession)
                return new DriverResult(21, "����� ��� �������", DriverResultType.Error);

            if(string.IsNullOrEmpty(userName))
                return new DriverResult(10, "��� ������� ������������ ����� ������ ���� ������", DriverResultType.Error);

            _isOpenSession = true;
            _openSessionUserName = userName;
            _openSessionDateTime = DateTime.Now;

            return new DriverResult(0, "������ ���", DriverResultType.Information);
        }

        /// <summary>
        /// ������� �����
        /// </summary>
        /// <returns><see cref="DriverResult"/> - ���������</returns>
        public DriverResult CloseSession(string userName=null)
        {

            if (!_isOpenSession)
            {
                SetUserName(userName);
                return new DriverResult(22, "����� �� �������", DriverResultType.Error);
            }

            _isOpenSession = false;

            return new DriverResult(0, "������ ���", DriverResultType.Information);
        }

        public DriverResult CashIncome(double cashValue, string userName = null)
        {
            var result = new List<string>();
            SetUserName(userName);
            GenerateStrings(result);

            result.Add($"��������     {cashValue}");

            CheckPrinted?.Invoke(this, new CheckPrintedEventArgs(result));
            return new DriverResult(0, "������ ���", DriverResultType.Information);
        }

        public DriverResult CashOutcome(double cashValue, string userName = null)
        {
            var result = new List<string>();
            SetUserName(userName);
            GenerateStrings(result);

            result.Add($"�������     {cashValue}");

            CheckPrinted?.Invoke(this, new CheckPrintedEventArgs(result));
            return new DriverResult(0, "������ ���", DriverResultType.Information);
        }

        public DriverResult XReport(string userName =null)
        {
            SetUserName(userName);
            var result = new List<string>();
            GenerateStrings(result);

            result.Add($"�������� ����� ��� �������");

            CheckPrinted?.Invoke(this, new CheckPrintedEventArgs(result));
            return new DriverResult(0, "������ ���", DriverResultType.Information);
        }

        private void GenerateStrings(List<string> result)
        {
            var company = _company;

            while (string.IsNullOrEmpty(company) == false)
            {
                var str = company.Substring(0, company.Length < _maxStringLength ? company.Length : _maxStringLength);

                result.Add(str);
                company = company.Substring(str.Length);
            }

            result.Add(_openSessionUserName);
        }

        public void SetUserName(string userName)
        {
            if(_openSessionUserName != userName && !string.IsNullOrEmpty(userName))
                _openSessionUserName = userName;
        }

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (CheckPrinted != null)
                foreach (var d in CheckPrinted.GetInvocationList())
                    CheckPrinted -= (d as EventHandler<CheckPrintedEventArgs>);
            _currentCheck = null;
            _openSessionUserName = null;
            GC.Collect();
        }

        public DriverResult RepeatLastCheck()
        {
            throw new NotImplementedException();
        }

        public ShtrihProperties GetProperties()
        {
            throw new NotImplementedException();
        }

        public DriverResult GetECRStatus(out string status)
        {
            throw new NotImplementedException();
        }

        public string GetUserName()
        {
            return _openSessionUserName;
        }

        public DriverResult PrintString(string testString)
        {
            throw new NotImplementedException();
        }

        public DriverResult CorrectionReceipt(CorrectionInfo info)
        {
            throw new NotImplementedException();
        }
    }
}