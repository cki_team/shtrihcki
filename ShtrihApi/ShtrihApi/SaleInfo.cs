using System;

namespace ShtrihApi
{
    /// <summary>
    /// ���������� �� �������
    /// </summary>
    public class SaleInfo
    {
        private double _tax;

        /// <summary>
        /// ����������
        /// </summary>
        public double Quantity { get; set; }

        /// <summary>
        /// ����
        /// </summary>
        public decimal Price { get; set; }


        /// <summary>
        /// ��������� ������ �� �������
        /// </summary>
        public decimal DiscountSum { get; set;}

        /// <summary>
        /// ������������
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// �����
        /// </summary>
        public double Tax
        {
            get { return _tax; }
            set
            {
                _tax = value;

                if (_tax < 0)
                    _tax = 0;
                else if (_tax > 18 && _tax <20)
                    _tax = 18;
                else if (_tax > 20)
                    _tax = 20;

                if (Equals(_tax, 0.0))
                    TaxObject = ShtrihApi.Tax.FreeTax;
                else if (Equals(_tax, 10.0))
                    TaxObject = ShtrihApi.Tax.Tax10;
                else if (Equals(_tax, 18.0))
                    TaxObject = ShtrihApi.Tax.Tax18;
                else if (Equals(_tax, 20.0))
                    TaxObject = ShtrihApi.Tax.Tax20;


            }
        }

        /// <summary>
        /// ������ ������. ������������ �� ���������
        /// </summary>
        public Tax TaxObject { get; set; }

        /// <summary>
        /// ������� ������� �������
        /// </summary>
        public PaymentTypeSign PaymentTypeSign { get; set; } = PaymentTypeSign.FullPayment;
    }
}