using System;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace ShtrihApi
{
    /// <summary>
    /// ������� <see cref="IShtrikhApi"/>
    /// </summary>
    public class ShtrikhApiFactory
    {
        private void ApiOnCheckPrinted(object sender, CheckPrintedEventArgs checkPrintedEventArgs)
        {
            checkPrintedEventArgs.CheckText.ForEach(Console.WriteLine);
            MessageBox.Show(string.Join("\r\n", checkPrintedEventArgs.CheckText));
        }

        /// <summary>
        /// ����� ���� �� �������
        /// </summary>
        /// <param name="port">���� ����������</param>
        /// <param name="debugMode">����-�����</param>
        /// <param name="timeout">�������� � �� � �������, �������� ����� ��������� �����������</param>
        /// <param name="paperWidth">������������ ������ �����</param>
        /// <param name="ip">Ip ����������</param>
        /// <returns></returns>
        public IShtrikhApi ApiCreate(bool debugMode = true, int timeout = 1000, string ip = null, int port = 0, int paperWidth = 48)
        {
            if (debugMode)
            {
                DialogResult result = MessageBox.Show(
                    "������ � ����������� ����� � �������� ������.\r\n" +
                    "����� ������, ���������� ��������� ������ � ������� ������", "��������",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                if (result == DialogResult.OK)
                {
                    DialogResult result1 = MessageBox.Show(
                        "�� �������������, ��� ����� �������� ��� � �������� ������!\r\n" +
                        "������� ������, ����� ���������� ������ � ������� ������", "��������",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                    if (result1 == DialogResult.OK)
                    {
                        var api = new ConsoleShtrikhApi("��� �������", "���������� �. 25");

                        api.CheckPrinted += ApiOnCheckPrinted;

                        return api;
                    }
                }
            }

            return new DriverShtrikhApi(ip, port) { Timeout = timeout, MaxStringLength = paperWidth };

        }

        /// <summary>
        /// ��������������� �������� ��� �������� � ������� ������ 48
        /// </summary>
        /// <returns></returns>
        public IShtrikhApi CreateWidth48(string ip = null, int port = 0)
        {
            return new DriverShtrikhApi(ip, port) { MaxStringLength = 48 };
        }

        /// <summary>
        /// ��������������� �������� ��� �������� � ������� ������ 32
        /// </summary>
        /// <returns></returns>
        public IShtrikhApi CreateWidth32(string ip = null, int port = 0)
        {
            return new DriverShtrikhApi(ip, port) { MaxStringLength = 32 };
        }

        /// <summary>
        /// ���������� ����� ���
        /// </summary>
        /// <param name="checkInfo"></param>
        /// <returns></returns>
        public Check CheckCreate(CheckInfo checkInfo)
        {
            return new Check(checkInfo);
        }

    }
}