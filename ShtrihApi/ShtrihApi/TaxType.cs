namespace ShtrihApi
{
    public enum TaxType
    {
        NoTax,
        Tax10,
        Tax18
    }
}