using System;
using System.Threading;

namespace ShtrihApi
{
    public class WaitOne
    {
        static readonly AutoResetEvent AutoEvent = new AutoResetEvent(false);

        public static bool DoWork(Action action, int timeout)
        {
            ThreadPool.QueueUserWorkItem(
                new WaitCallback(state =>
                {
                    action();
                    ((AutoResetEvent)state).Set();
                }), AutoEvent);

            return AutoEvent.WaitOne(timeout);
        }
    }
}