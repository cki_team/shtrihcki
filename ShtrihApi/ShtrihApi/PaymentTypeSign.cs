using System.ComponentModel;

namespace ShtrihApi
{
    /// <summary>
    /// ������� ������� �������
    /// </summary>
    public enum PaymentTypeSign
    {
        /// <summary>
        /// ���������� 100%
        /// </summary>
        [Description("���������� 100%")]
        Prepayment100 = 1,
        /// <summary>
        /// ��������� ����������
        /// </summary>
        [Description("��������� ����������")]
        PartialPrepayment = 2,
        /// <summary>
        /// �����
        /// </summary>
        [Description("�����")]
        InterjacentPayment = 3,
        /// <summary>
        /// ������ ������
        /// </summary>
        [Description("������ ������")]
        FullPayment = 4,
        /// <summary>
        /// ��������� ������ � ������
        /// </summary>
        [Description("��������� ������ � ������")]
        PartialSettlementAndCredit = 5,
        /// <summary>
        /// �������� � ������
        /// </summary>
        [Description("�������� � ������")]
        TransfeCredit = 6,
        /// <summary>
        /// ������ �������
        /// </summary>
        [Description("������ �������")]
        CreditPayment = 7
    }
}