using System;

namespace ShtrihApi
{
    public class Tax
    {
        public static readonly Tax FreeTax = new Tax("��� ���", TaxValue.FreeTax);
        public static readonly Tax Tax0 = new Tax(" 0%", TaxValue.Tax0);
        public static readonly Tax Tax10 = new Tax(" 10%", TaxValue.Tax10);
        public static readonly Tax Tax18 = new Tax(" 18%", TaxValue.Tax18);
        public static readonly Tax Tax20 = new Tax(" 20%", TaxValue.Tax20);
        public static readonly Tax Tax10110 = new Tax("10/110", TaxValue.Tax10110);
        public static readonly Tax Tax18180 = new Tax("18/118", TaxValue.Tax18118);
        public static readonly Tax Tax20120 = new Tax("20/120", TaxValue.Tax20120);

        public string Name { get; }

        public TaxValue Value { get; }

        public int Percentage { get; }

        public Tax(string name, TaxValue value)
        {
            Name = name;
            Value = value;

            switch (Value)
            {
                case TaxValue.Tax0:
                    Percentage = 0;
                    break;
                case TaxValue.Tax10:
                    Percentage = 10;
                    break;
                case TaxValue.Tax18:
                    Percentage = 18;
                    break;
                case TaxValue.Tax20:
                    Percentage = 20;
                    break;
                case TaxValue.Tax10110:
                    Percentage = 10;
                    break;
                case TaxValue.Tax18118:
                    Percentage = 18;
                    break;
                case TaxValue.Tax20120:
                    Percentage = 20;
                    break;
                case TaxValue.FreeTax:
                    Percentage = 0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override bool Equals(object obj)
        {
            var tax = obj as Tax;

            if (tax == null)
                return false;

            return Name == tax.Name && Value == tax.Value;
        }
    }
}