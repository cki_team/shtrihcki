using System;
using System.Collections;
using System.Collections.Generic;

namespace ShtrihApi
{
    /// <summary>
    /// ���, ��������� �� ������
    /// </summary>
    public class Check : IEnumerable<Sale>
    {
        /// <summary>
        /// ���������� �� ����
        /// </summary>
        public CheckInfo Info { get; }

        private List<Sale> _sales;

        /// <summary>
        /// �������� ����
        /// </summary>
        /// <param name="checkInfo">���������� �� ����</param>
        public Check(CheckInfo checkInfo)
        {
            _sales = new List<Sale>();

            Info = checkInfo;
        }

        /// <summary>
        /// �������� ������� � ���
        /// </summary>
        /// <param name="saleInfo"></param>
        /// <returns></returns>
        public Sale Add(SaleInfo saleInfo)
        {
            var sale = new Sale(saleInfo);

            _sales.Add(sale);

            return sale;
        }

        /// <summary>
        /// ������ ������� �� ����
        /// </summary>
        /// <param name="sale"></param>
        public void Remove(Sale sale)
        {
            if (sale == null) throw new ArgumentNullException(nameof(sale));
            _sales.Remove(sale);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<Sale> GetEnumerator()
        {
            return _sales.GetEnumerator();
        }
    }
}