﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using DrvFRLib;

namespace ShtrihApi
{
    /// <summary>
    /// Работа с драйвером ФР 4.14 ШТРИХ-М
    /// </summary>
    internal class DriverShtrikhApi : IShtrikhApi, IDisposable
    {
        private readonly List<Tax> _taxes = new List<Tax>
        {
            Tax.FreeTax,
            Tax.Tax0,
            Tax.Tax10,
            Tax.Tax18,
            Tax.Tax20,
            Tax.Tax10110,
            Tax.Tax18180,
            Tax.Tax20120
        };

        private Dictionary<Tax, int> _taxValues;
        protected static DrvFR Driver { get; private set; }
        private static string _openSessionUserName;

        /// <summary>
        /// Maximum width of the cash paper
        /// </summary>
        public int MaxStringLength { get; set; } = 10;

        /// <summary>
        /// 
        /// </summary>
        public int AdminPassword { get; set; } = 30;

        /// <summary>
        /// Количество дней оставшихся до конца ФН, при котором будет генерироваться ResultCodeDescription о том, что следует заменить ФН
        /// </summary>
        public int ExpireDate { get; set; } = 30;

        /// <summary>
        /// Количество дней без связи с ОФД, при котором будет генерироваться ResultCodeDescription о том, что следует проверить связь
        /// </summary>
        public int OfdExpireDate { get; set; } = 1;

        /// <summary>
        /// Сколько миллисекунд ждать ответа от ККТ
        /// </summary>
        public int Timeout { get; set; } = 1000;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public DriverShtrikhApi(string ip = null, int port = 0)
        {
            if (ip != null)
            {
                Driver = new DrvFR();

                if (Driver.IPAddress != ip || Driver.TCPPort != port)
                {
                    Driver.Disconnect();

                    Driver.TCPPort = port;
                    Driver.IPAddress = ip;
                    Driver.UseIPAddress = true;
                    Driver.ConnectionType = 6;

                    Driver.Connect();

                    if (Driver.ResultCode != 0)
                        throw new ShtrihApiException($"Не удалось подключиться. ({Driver.ResultCodeDescription})");
                }
            }
            else if (Driver == null)
                Driver = new DrvFR();

            CheckConnection();

            Init();
        }

        private void Init()
        {
            _taxValues = new Dictionary<Tax, int>();

            Driver.Password = AdminPassword;

            Driver.TableNumber = 6;
            Driver.GetTableStruct();

            var taxCount = Driver.RowNumber;

            for (int i = 1; i < taxCount + 1; i++)
            {
                Driver.TableNumber = 6;
                Driver.RowNumber = i;
                Driver.FieldNumber = 2;
                Driver.GetFieldStruct();

                Driver.ReadTable();

                if (string.IsNullOrWhiteSpace(Driver.ValueOfFieldString))
                    continue;

                Tax tax = _taxes.FirstOrDefault(x => Driver.ValueOfFieldString.Contains(x.Name));

                if (tax == null)
                    throw new ShtrihApiException($"В ККМ налога '{Driver.ValueOfFieldString}' нет. Обратитесь к администратору.");

                _taxValues[tax] = i;
            }
        }

        /// <summary>
        /// Получить характеристики устройства
        /// </summary>
        /// <returns></returns>
        public ShtrihProperties GetProperties()
        {
            CheckConnection();

            Driver.Password = AdminPassword;

            Driver.ReadSerialNumber();
            var kktSerial = Driver.SerialNumber;

            Driver.FNGetFiscalizationResult();
            var kktReg = Driver.KKTRegistrationNumber;

            Driver.FNGetSerial();
            var fnSerial = Driver.SerialNumber;

            Driver.FNGetStatus();
            var docNumber = Driver.DocumentNumber;

            Driver.FNFindDocument();

            string docType = null;
            switch (Driver.DocumentType)
            {
                case 1:
                    docType = "Отчёт о регистрации";
                    break;
                case 2:
                    docType = "Отчёт об открытии смены";
                    break;
                case 3:
                    docType = "Кассовый чек";
                    break;
                case 4:
                    docType = "БСО";
                    break;
                case 5:
                    docType = "Отчёт о закрытии смены";
                    break;
                case 6:
                    docType = "Отчёт о закрытии фискального накопителя";
                    break;
                case 7:
                    docType = "Подтверждение оператора";
                    break;
                case 11:
                    docType = "Отчёт об изменении параметров регистрации";
                    break;
                case 21:
                    docType = "Отчет о состоянии расчетов";
                    break;
                case 31:
                    docType = "Кассовый чек коррекции";
                    break;
                case 41:
                    docType = "Бланк строгой отчетности коррекции";
                    break;
            }

            return new ShtrihProperties()
            {
                DocumentType = docType,
                DocumentNumber = docNumber.ToString(),
                FnSerialNumber = fnSerial,
                KktRegNumber = kktReg,
                KktSerialNumber = kktSerial
            };
        }

        public DriverResult XReport(string userName = null)
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            SetUserName(userName);

            Driver.PrintReportWithoutCleaning();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        public DriverResult ShowProperties()
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.ShowProperties();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        public DriverResult SendCheck(Check check)
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.Password = AdminPassword;

            if (string.IsNullOrEmpty(check.Info.UserName))
            {
                return new DriverResult(-1, "Имя кассира должно быть задано", DriverResultType.Error);
            }

            if (check.Info.CashValue < 0)
                return new DriverResult(-1, "Сумма наличных не может быть отрицательной", DriverResultType.Error);

            Driver.GetECRStatus();

            if (Driver.ECRMode == 3)
            {
                return new DriverResult(-1, "Следует закрыть смену, прежде чем продолжить", DriverResultType.Error);
            }

            if (Driver.ECRMode == 4)
            {
                DriverResult openResult = OpenSession(check.Info.UserName);

                if (openResult.ResultCode != 0)
                    return openResult;
            }

            if (Driver.ECRMode == 8)
            {
                return new DriverResult(-1, "Есть открытый документ. Продолжите печать или аннулируйте его.", DriverResultType.Error);
            }

            Driver.CheckType = (int)check.Info.CheckType;

            if (check.Info.ImagePath != null && check.Info.ImageDockType == ImageDockType.Top)
            {
                var drawResult = DrawImage(check);
                if (drawResult.ResultCode != 0)
                    return drawResult;
            }

            SetUserName(check.Info.UserName);

            Driver.TableNumber = 18;
            Driver.GetTableStruct();
            Driver.FieldNumber = 9;
            Driver.GetFieldStruct();
            var result1 = Driver.ReadTable();
            var address = Driver.ValueOfFieldString;

            Driver.TableNumber = 18;
            Driver.GetTableStruct();
            Driver.FieldNumber = 7;
            Driver.GetFieldStruct();
            var result2 = Driver.ReadTable();
            var company = Driver.ValueOfFieldString;

            Driver.OpenCheck();

            int i = 5;

            while (Driver.ResultCode == 80 && i != 0)
            {
                Thread.Sleep(200);
                Driver.OpenCheck();
                i++;
            }

            if (Driver.ResultCode != 0)
            {
                return new DriverResult(Driver.ResultCode, $"Открытие чека: {Driver.ResultCodeDescription}",
                   Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
            }

            if (string.IsNullOrWhiteSpace(check.Info.SaleNumber) == false)
            {
                Driver.StringForPrinting = $"Продажа №{check.Info.SaleNumber}";
                Driver.PrintString();
            }

            while (string.IsNullOrEmpty(company) == false)
            {
                var str = company.Substring(0, company.Length < MaxStringLength ? company.Length : MaxStringLength);

                Driver.StringForPrinting = str;
                Driver.PrintString();
                company = company.Substring(str.Length);
            }

            while (string.IsNullOrEmpty(address) == false)
            {
                var str = address.Substring(0, address.Length < MaxStringLength ? address.Length : MaxStringLength);

                Driver.StringForPrinting = str;
                Driver.PrintString();
                address = address.Substring(str.Length);
            }

            if (check.Info.Email != null)
            {
                Driver.CustomerEmail = check.Info.Email;
                Driver.FNSendCustomerEmail();
            }

            decimal sumSale = 0;

            for (var index = 0; index < check.Count(); index++)
            {
                var sale = check.ElementAt(index);
                try
                {
                    sumSale += AddSaleToCheck(sale, index + 1);
                }
                catch (ShtrihApiException e)
                {
                    return new DriverResult(-1, $"Добавление покупки в чек: {e.Message}", DriverResultType.Error);
                }
            }

            Driver.StringForPrinting = new string('=', MaxStringLength);
            Driver.PrintString();
            Driver.StringForPrinting = null;

            var discountSum = check.Sum(x => x.Info.DiscountSum);
            if (discountSum > 0 && check.Info.CheckType == CheckType.Sale)
            {
                Driver.StringForPrinting = $"ВАША СКИДКА СОСТАВИЛА {discountSum:C2}";
                Driver.PrintString();
            }

            if (check.Info.ShouldRound && sumSale > 1)
            {
                Driver.DiscountOnCheck = (double)sumSale % check.Info.Round;
            }
            else
            {
                Driver.DiscountOnCheck = 0;
            }

            if (check.Info.PaymentType == PaymentType.Cash)
            {
                Driver.Summ1 = check.Info.CashValue;
                Driver.Summ2 = 0;
            }
            else if (check.Info.PaymentType == PaymentType.CreditCard)
            {
                Driver.Summ1 = 0;
                Driver.Summ2 = sumSale - (decimal)Driver.DiscountOnCheck;
            }
            else
            {
                if (check.Info.CreditCardValue > sumSale)
                {
                    Driver.Summ1 = 0;
                    Driver.Summ2 = sumSale;
                }
                else
                {
                    Driver.Summ1 = check.Info.CashValue;
                    Driver.Summ2 = check.Info.CashValue > sumSale ? 0 : sumSale - check.Info.CashValue - (decimal)Driver.DiscountOnCheck;
                }
            }

            Driver.Summ3 = 0;
            Driver.Summ4 = 0;
            //_driver.DiscountOnCheck = check.Info.DiscountOnCheck;
            Driver.CloseCheck();

            Driver.Password = AdminPassword;

            if (Driver.ResultCode == 0)
            {
                Driver.FNGetInfoExchangeStatus();

                if (Driver.MessageCount > 0 && (DateTime.Now - Driver.Date).Days > OfdExpireDate)
                {
                    string message =
                        "Есть документы не переданные в офд. Возможно проблемы со связью.\r\nОбратитесь к администратору.";
                    message += $"Количество документов: {Driver.MessageCount}\r\n";
                    message += $"Номер документа: {Driver.DocumentNumber}\r\n";
                    message += $"Дата и время документа : {Driver.Date.Add(new TimeSpan(Driver.Time.TimeOfDay.Ticks))}\r\n";

                    return new DriverResult(0, message, DriverResultType.Warning);
                }
            }

            return new DriverResult(Driver.ResultCode, $"Закрытие чека: {Driver.ResultCodeDescription}",
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        /// <summary>
        /// Повторить последний чек
        /// </summary>
        /// <returns></returns>
        public DriverResult RepeatLastCheck()
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.RepeatDocument();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        public DriverResult CancelCheck()
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.CancelCheck();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        public DriverResult ContinueLastCheck()
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.ContinuePrint();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        /// <summary>
        /// Проверка связи с устройством
        /// </summary>
        /// <exception cref="Exception">Нет связи</exception>
        public void CheckConnection()
        {
            lock (Driver)
            {
                Thread currentThread = Thread.CurrentThread;

                Thread thread = new Thread(() => Driver.CheckConnection());
                thread.Start();

                for (int i = 100; i < Timeout; i += 100)
                {
                    currentThread.Join(i);

                    if (thread.ThreadState == ThreadState.Stopped)
                    {
                        if (Driver.ResultCode == -1)
                            throw new ShtrihApiException(Driver.ResultCodeDescription);

                        return;
                    }
                }

                if (thread.ThreadState == ThreadState.Running)
                {
                    thread.Abort();
                    throw new ShtrihApiException("Нет связи");
                }
            }

            if (Driver.ResultCode == -1)
                throw new ShtrihApiException(Driver.ResultCodeDescription);

            //var result = WaitOne.DoWork(() => _driver.CheckConnection(), Timeout);
            //if (result == false)
            //    throw new Exception("Нет связи");


        }

        private decimal AddSaleToCheck(Sale sale, int index)
        {
            //регистрация операции
            Driver.Quantity = sale.Info.Quantity;
            Driver.Price = sale.Info.Price;

            Driver.Tax1 = _taxValues[sale.Info.TaxObject];
            Driver.Tax2 = 0;
            Driver.Tax3 = 0;
            Driver.Tax4 = 0;
            Driver.StringForPrinting = $"//{sale.Info.Name}";
            //Driver.PaymentTypeSign = (int)sale.Info.PaymentTypeSign;
            Driver.FNDiscountOperation();

            var price = sale.Info.Price * (decimal)sale.Info.Quantity;
            double tax = sale.Info.TaxObject.Percentage;

            //печать

            double taxValue = Math.Round((double)price / (1 + tax / 100) * tax / 100, 2);

            string pointers = new string('.', MaxStringLength);

            string titleString = $"{index}.Наименование ";

            string firstName = sale.Info.Name.Length > MaxStringLength - titleString.Length
                ? sale.Info.Name.Substring(0, MaxStringLength - titleString.Length)
                : sale.Info.Name;

            string name = sale.Info.Name.Length > MaxStringLength - titleString.Length
                ? sale.Info.Name.Substring(firstName.Length)
                : null;

            List<string> names = new List<string>
            {
                $"{titleString}{firstName}"
            };

            while (string.IsNullOrEmpty(name) == false)
            {
                var str = name.Substring(0, name.Length < MaxStringLength ? name.Length : MaxStringLength);

                names.Add(str);
                name = name.Substring(str.Length);
            }

            Driver.CarryStrings = true; //не ясно как работает
            names.ForEach(x =>
            {
                Driver.StringForPrinting = x;
                Driver.PrintString();
            });

            string priceString = $"{sale.Info.Price:N2}";
            Driver.StringForPrinting = $"Цена{pointers.Substring(0, MaxStringLength - 4 - priceString.Length)}{priceString}";
            Driver.PrintString();

            string quantityString = $"{sale.Info.Quantity}";
            Driver.StringForPrinting = $"Количество{pointers.Substring(0, MaxStringLength - 10 - quantityString.Length)}{quantityString}";
            Driver.PrintString();

            string description = DescriptionAttr(sale.Info.PaymentTypeSign);
            Driver.StringForPrinting = description;
            Driver.PrintString();

            if (taxValue > 0.00)
            {
                string taxString = $"{taxValue:N2}";
                Driver.StringForPrinting =
                    $"НДС {tax}%{pointers.Substring(0, MaxStringLength - 7 - taxString.Length)}{taxString}";
                Driver.PrintString();
            }

            string sumString = $"{price:N2}";
            Driver.StringForPrinting = $"Стоимость{pointers.Substring(0, MaxStringLength - 9 - sumString.Length)}{sumString}";
            Driver.PrintString();

            return price;
        }

        private DriverResult DrawImage(Check check)
        {
            Driver.FileName = check.Info.ImagePath;
            Driver.FirstLineNumber = 1;
            Driver.LastLineNumber = 256;
            Driver.LoadImage();
            Driver.Draw();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        private static string DescriptionAttr<T>(T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }

        public DriverResult OpenSession(string userName)
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.Password = AdminPassword;

            Driver.FNGetExpirationTime();

            if (string.IsNullOrEmpty(userName))
            {
                return new DriverResult(-1, "Имя кассира должно быть задано", DriverResultType.Error);
            }

            DateTime expireDate = Driver.Date;

            SetUserName(userName);

            Driver.OpenSession();

            if (Driver.ResultCode == 0 && (expireDate - DateTime.Now).Days < ExpireDate)
                return new DriverResult(Driver.ResultCode, $"Срок действия ФН истекает через {(expireDate - DateTime.Now).Days} дней. Обратитесь к администратору для замены.", DriverResultType.Warning);
            else
                return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        public DriverResult CloseSession(string userName = null)
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            SetUserName(userName);

            Driver.PrintReportWithCleaning();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        public DriverResult CashIncome(double cashValue, string userName = null)
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.Password = AdminPassword;
            SetUserName(userName);

            Driver.Summ1 = (decimal)cashValue;

            Driver.CashIncome();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        public DriverResult CashOutcome(double cashValue, string userName = null)
        {
            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.Password = AdminPassword;

            SetUserName(userName);

            Driver.Summ1 = (decimal)cashValue;

            Driver.CashOutcome();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        /// <summary>
        /// Статус устройства
        /// </summary>
        /// <param name="status">статус</param>
        /// <returns></returns>
        public DriverResult GetECRStatus(out string status)
        {
            status = null;

            try
            {
                CheckConnection();
            }
            catch (ShtrihApiException e)
            {
                return new DriverResult(-1, e.Message, DriverResultType.Error);
            }

            Driver.Password = AdminPassword;

            Driver.GetECRStatus();

            status += $"Режим: {Driver.ECRMode} ";

            switch (Driver.ECRMode)
            {
                case 0:
                    status += "Принтер в рабочем режиме";
                    break;
                case 1:
                    status += "Выдача данных";
                    break;
                case 2:
                    status += "Открытая смена, 24 часа не кончились ";
                    break;
                case 3:
                    status += "Открытая смена, 24 часа кончились";
                    break;
                case 4:
                    status += "Закрытая смена";
                    break;
                case 5:
                    status += "Блокировка по неправильному паролю налогового инспектора";
                    break;
                case 6:
                    status += "Ожидание подтверждения ввода даты";
                    break;
                case 7:
                    status += "Разрешение изменения положения десятичной точки";
                    break;
                case 8:
                    status += "Открытый документ";
                    break;
                case 9:
                    status += "Режим разрешения технологического обнуления";
                    break;
                case 10:
                    status += "Тестовый прогон";
                    break;
                case 11:
                    status += "Печать полного фискального отчета";
                    break;
                case 12:
                    status += "Печать длинного отчета ЭКЛЗ";
                    break;
                case 13:
                    status += "Работа с фискальным подкладным документом";
                    break;
                case 14:
                    status += "Печать подкладного документа";
                    break;
                case 15:
                    status += "Фискальный подкладной документ сформирован";
                    break;
            }

            status += $"\r\nПодрежим: {Driver.ECRAdvancedMode} ";

            switch (Driver.ECRAdvancedMode)
            {
                case 0:
                    status += "Бумага есть – ККТ не в фазе печати операции";
                    break;
                case 1:
                    status += "Пассивное отсутствие бумаги – ККМ не в фазе печати операции";
                    break;
                case 2:
                    status += "Активное отсутствие бумаги – ККМ в фазе печати операции";
                    break;
                case 3:
                    status += "После активного отсутствия бумаги – ККМ ждет команду продолжения печати";
                    break;
                case 4:
                    status += "Фаза печати операции длинного отчета";
                    break;
                case 5:
                    status += "Фаза печати операции";
                    break;
            }

            status += "\r\n";

            status += $"Номер документа : {Driver.OpenDocumentNumber}\r\n";
            status += $"Рычаг термоголовки чека опущен: {(Driver.ReceiptRibbonLever ? "Да" : "Нет")}\r\n";
            status += $"Рычаг термоголовки журнала опущен: {(Driver.JournalRibbonLever ? "Да" : "Нет")}\r\n";
            status += $"Оптический датчик чека: {(Driver.ReceiptRibbonOpticalSensor ? "Да" : "Нет")}\r\n";
            status += $"Оптический датчик журнала : {(Driver.JournalRibbonOpticalSensor ? "Да" : "Нет")}\r\n";
            status += $"Рулон чековой ленты: {(Driver.ReceiptRibbonIsPresent ? "Да" : "Нет")}\r\n";
            status += $"Рулон контрольной ленты  : {(Driver.JournalRibbonIsPresent ? "Да" : "Нет")}\r\n";

            Driver.FNGetInfoExchangeStatus();

            status += $"Количество не переданных сообщений для ОФД : {Driver.MessageCount}\r\n";
            status += $"Номер документа для ОФД первого в очереди : {Driver.DocumentNumber}\r\n";
            status += $"Дата и время документа для ОФД первого в очереди : {Driver.Date}\r\n";

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        public void SetUserName(string userName)
        {
            if (_openSessionUserName != userName && !string.IsNullOrEmpty(userName))
            {
                _openSessionUserName = userName;
                Driver.TableNumber = 2;
                Driver.RowNumber = 30;
                Driver.FieldNumber = 2;
                Driver.ValueOfFieldString = userName;
                Driver.GetFieldStruct();
                Driver.WriteTable();
            }
        }

        /// <summary>
        /// Текущее имя кассира
        /// </summary>
        /// <returns></returns>
        public string GetUserName()
        {
            if (_openSessionUserName == null)
            {
                Driver.TableNumber = 2;
                Driver.RowNumber = 30;
                Driver.FieldNumber = 2;
                Driver.GetFieldStruct();
                var result = Driver.ReadTable();
                _openSessionUserName = Driver.ValueOfFieldString;
            }

            return _openSessionUserName;
        }

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            Driver.Disconnect();
            Driver = null;
            _openSessionUserName = null;
            GC.Collect();
        }

        /// <summary>
        /// Печать тестовой строки на ленте
        /// </summary>
        /// <param name="testString">Тестовая строка</param>
        /// <returns></returns>
        public DriverResult PrintString(string testString)
        {
            Driver.StringForPrinting = testString;
            Driver.PrintString();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }

        /// <summary>
        /// Печать чека коррекции
        /// </summary>
        /// <param name="info">Информация по чеку коррекции</param>
        /// <returns></returns>
        public DriverResult CorrectionReceipt(CorrectionInfo info)
        {
            Driver.Password = AdminPassword;
            Driver.CorrectionType = (int)info.CorrectionType;
            Driver.CalculationSign = (int)info.CorrectionSign;
            Driver.Summ1 = info.ResultAmount;
            Driver.Summ2 = info.CashAmount;
            Driver.Summ3 = info.CreditAmount;
            Driver.Summ4 = info.PrepayAmount;
            Driver.Summ5 = info.PostpayAmount;
            Driver.Summ7 = info.VAT18Amount;
            Driver.Summ8 = info.VAT0Amount;
            Driver.Summ9 = info.VAT0Amount;
            Driver.Summ10 = info.NoVATAmount;
            Driver.Summ11 = info.VAT18118Amount;
            Driver.Summ12 = info.VAT10110Amount;

            if (info.ChequeNumber != 0)
                Driver.ReceiptNumber = info.ChequeNumber;

            if (info.DocumentNumber != 0)
                Driver.DocumentNumber = info.DocumentNumber;

            Driver.FNBuildCorrectionReceipt2();

            return new DriverResult(Driver.ResultCode, Driver.ResultCodeDescription,
                Driver.ResultCode == 0 ? DriverResultType.Information : DriverResultType.Error);
        }
    }
}
