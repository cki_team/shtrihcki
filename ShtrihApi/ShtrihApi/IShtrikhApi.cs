﻿using System;

namespace ShtrihApi
{
    /// <summary>
    /// Управление кассовым аппаратом
    /// </summary>
    public interface IShtrikhApi : IDisposable
    {
        /// <summary>
        /// Аннулировать чек
        /// </summary>
        /// <returns><see cref="DriverResult"/> - результат</returns>
        DriverResult CancelCheck();

        /// <summary>
        /// Продолжить печать
        /// </summary>
        /// <returns><see cref="DriverResult"/> - результат</returns>
        DriverResult ContinueLastCheck();

        /// <summary>
        /// Повторить последний чек
        /// </summary>
        /// <returns></returns>
        DriverResult RepeatLastCheck();

        /// <summary>
        /// Распечатать чек
        /// </summary>
        /// <param name="check">Чек</param>
        /// <returns><see cref="DriverResult"/> - результат</returns>
        DriverResult SendCheck(Check check);

        /// <summary>
        /// Закрыть смену
        /// </summary>
        /// <returns><see cref="DriverResult"/> - результат</returns>
        DriverResult OpenSession(string userName);

        /// <summary>
        /// Закрыть смену
        /// </summary>
        /// <returns><see cref="DriverResult"/> - результат</returns>
        DriverResult CloseSession(string userName = null);

        /// <summary>
        /// Добавить деньги в кассу
        /// </summary>
        /// <param name="cashValue">Количество</param>
        /// <param name="userName">Имя кассира</param>
        /// <returns></returns>
        DriverResult CashIncome(double cashValue, string userName = null);

        /// <summary>
        /// Вывод денег из кассы
        /// </summary>
        /// <param name="cashValue">Количество</param>
        /// <param name="userName">Имя кассира</param>
        /// <returns></returns>
        DriverResult CashOutcome(double cashValue, string userName=null);

        /// <summary>
        /// X отчет
        /// </summary>
        /// <returns></returns>
        DriverResult XReport(string userName = null);

        /// <summary>
        /// Задать имя пользователя 
        /// </summary>
        /// <param name="userName"></param>
        void SetUserName(string userName);

        /// <summary>
        /// Завершить работу с драйвером
        /// </summary>
        void Close();

        /// <summary>
        /// Получить характеристики устройства
        /// </summary>
        /// <returns></returns>
        ShtrihProperties GetProperties();

        /// <summary>
        /// Статус устройства
        /// </summary>
        /// <param name="status">статус</param>
        /// <returns></returns>
        DriverResult GetECRStatus(out string status);

        /// <summary>
        /// Текущее имя кассира
        /// </summary>
        /// <returns></returns>
        string GetUserName();

        /// <summary>
        /// Печать тестовой строки на ленте
        /// </summary>
        /// <param name="testString">Тестовая строка</param>
        /// <returns></returns>
        DriverResult PrintString(string testString);

        /// <summary>
        /// Печать чека коррекции
        /// </summary>
        /// <param name="info">Информация по чеку коррекции</param>
        /// <returns></returns>
        DriverResult CorrectionReceipt(CorrectionInfo info);
    }
}