namespace ShtrihApi
{
    /// <summary>
    /// ���������� �� ����
    /// </summary>
    public class CheckInfo
    {
        private double _discountOnCheck;

        /// <summary>
        /// ��� �������� ���
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// �������/����� ����������
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// ��� ����
        /// </summary>
        public CheckType CheckType { get; set; } = CheckType.Sale;

        /// <summary>
        /// ��� ������
        /// </summary>
        public PaymentType PaymentType { get; set; } = PaymentType.CreditCard;

        /// <summary>
        /// ������ ������ ���������
        /// </summary>
        public decimal CashValue { get; set; }

        /// <summary>
        /// ������ ������ ���������
        /// </summary>
        public decimal CreditCardValue { get; set; }

        /// <summary>
        /// ������������ ����������
        /// </summary>
        public bool ShouldRound { get; set; } = true;

        /// <summary>
        /// ����������
        /// </summary>
        public double Round { get; set; } = 0.5;

        /// <summary>
        /// ������ �� ����
        /// </summary>
        public double DiscountOnCheck
        {
            get { return _discountOnCheck; }
            set
            {
                if (value < 0)
                    _discountOnCheck = 0;
                else if (value >= 100)
                    _discountOnCheck = 99.99;
                else
                    _discountOnCheck = value;
            }
        }

        /// <summary>
        /// ���� � ����
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// ������������ ����
        /// </summary>
        public ImageDockType ImageDockType { get; set; }

        /// <summary>
        /// ����� �������
        /// </summary>
        public string SaleNumber { get; set; }
    }
}