namespace ShtrihApi
{
    /// <summary>
    /// �������������� ����������
    /// </summary>
    public class ShtrihProperties
    {
        /// <summary>
        /// ��������������� ����� ���
        /// </summary>
        public string KktRegNumber { get; set; }

        /// <summary>
        /// �������� ����� ���
        /// </summary>
        public string KktSerialNumber { get; set; }

        /// <summary>
        /// �������� ����� ����������� ����������
        /// </summary>
        public string FnSerialNumber { get; set; }

        /// <summary>
        /// ����� ���������� ���������
        /// </summary>
        public string DocumentNumber { get; set; }

        /// <summary>
        /// ��� ���������� ���������
        /// </summary>
        public string DocumentType { get; set; }
    }
}