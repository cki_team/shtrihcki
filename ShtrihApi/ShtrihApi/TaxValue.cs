namespace ShtrihApi
{
    public enum TaxValue
    {
        Tax0,
        Tax10,
        Tax18,
        Tax20,
        Tax10110,
        Tax18118,
        Tax20120,
        FreeTax
    }
}