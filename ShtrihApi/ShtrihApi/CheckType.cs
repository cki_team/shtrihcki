using System.ComponentModel;

namespace ShtrihApi
{
    public enum CheckType
    {
        /// <summary>
        /// �������
        /// </summary>
        [Description("�������")]
        Sale = 0,
        /// <summary>
        /// �������
        /// </summary>
        [Description("�������")]
        Buy = 1,
        /// <summary>
        /// ������� �������
        /// </summary>
        [Description("������� �������")]
        ReturnSale = 2,
        /// <summary>
        /// ������� �������
        /// </summary>
        [Description("������� �������")]
        ReturnBuy = 3
    }
}