using System;
using ShtrihApi;

namespace DriverShtrikhApi
{
    /// <summary>
    /// ������� <see cref="IShtrikhApi"/>
    /// </summary>
    public class ShtrikhApiFactory
    {
        /// <summary>
        /// ����� ���� �� �������
        /// </summary>
        /// <param name="company">�������� �������� ��������</param>
        /// <param name="address">����� �������� ��������</param>
        /// <returns></returns>
        public IShtrikhApi ConsoleApiCreate(string company, string address)
        {
            var api = new ShtrikhApi(company, address);

            api.CheckPrinted += ApiOnCheckPrinted;

            return api;
        }

        private void ApiOnCheckPrinted(object sender, CheckPrintedEventArgs checkPrintedEventArgs)
        {
            checkPrintedEventArgs.CheckText.ForEach(Console.WriteLine);
            foreach (string s in checkPrintedEventArgs.CheckText)
            {
                MessageBox.Show(s);
            }
        }
    }
}