﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DrvFRLib;
using ShtrihApi;

namespace DriverShtrikhApi
{
    public class DriverShtrikhApi : IShtrikhApi
    {
        private readonly Dictionary<int, int> _taxValues;
        private static DrvFR _driver;

        /// <summary>
        /// Maximum width of the cash paper
        /// </summary>
        public int MaxStringLength { get; set; } = 32;

        /// <summary>
        /// 
        /// </summary>
        public int AdminPassword { get; set; } = 30;

        /// <summary>
        /// Сколько секунд ждать ответа от ККТ
        /// </summary>
        public int Timeout { get; set; } = 1;
        
        public DriverShtrikhApi()
        {
            if (_driver == null)
                _driver = new DrvFR();

            CheckConnection();

            _taxValues = new Dictionary<int, int>();

            _driver.Password = AdminPassword;

            _driver.TableNumber = 6;
            _driver.GetTableStruct();

            var taxCount = _driver.RowNumber;

            for (int i = 1; i < taxCount; i++)
            {
                _driver.TableNumber = 6;
                _driver.RowNumber = i;
                _driver.FieldNumber = 1;
                _driver.GetFieldStruct();

                _driver.ReadTable();

                if (_taxValues.ContainsKey(_driver.ValueOfFieldInteger) == false)
                    _taxValues.Add(_driver.ValueOfFieldInteger, i);
            }
        }

        public ShtrihProperties GetProperties()
        {
            CheckConnection();

            _driver.Password = AdminPassword;

            _driver.ReadSerialNumber();
            var kktSerial = _driver.SerialNumber;

            _driver.FNGetFiscalizationResult();
            var kktReg = _driver.KKTRegistrationNumber;

            _driver.FNGetSerial();
            var fnSerial = _driver.SerialNumber;

            _driver.FNGetStatus();
            var docNumber = _driver.DocumentNumber;

            _driver.FNFindDocument();

            string docType = null;
            switch (_driver.DocumentType)
            {
                case 1:
                    docType = "Отчёт о регистрации";
                    break;
                case 2:
                    docType = "Отчёт об открытии смены";
                    break;
                case 3:
                    docType = "Кассовый чек";
                    break;
                case 4:
                    docType = "БСО";
                    break;
                case 5:
                    docType = "Отчёт о закрытии смены";
                    break;
                case 6:
                    docType = "Отчёт о закрытии фискального накопителя";
                    break;
                case 7:
                    docType = "Подтверждение оператора";
                    break;
                case 11:
                    docType = "Отчёт об изменении параметров регистрации";
                    break;
                case 21:
                    docType = "Отчет о состоянии расчетов";
                    break;
                case 31:
                    docType = "Кассовый чек коррекции";
                    break;
                case 41:
                    docType = "Бланк строгой отчетности коррекции";
                    break;
            }

            return new ShtrihProperties()
            {
                DocumentType = docType,
                DocumentNumber = docNumber.ToString(),
                FnSerialNumber = fnSerial,
                KktRegNumber = kktReg,
                KktSerialNumber = kktSerial
            };
        }

        public DriverResult XReport()
        {
            CheckConnection();

            _driver.PrintReportWithoutCleaning();

            return new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);
        }

        public DriverResult ZReport()
        {
            CheckConnection();

            _driver.PrintReportWithCleaning();

            return new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);
        }
        
        public DriverResult ShowProperties()
        {
            CheckConnection();

            _driver.ShowProperties();

            return new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);
        }

        public DriverResult SendCheck(Check check)
        {
            CheckConnection();

            _driver.Password = AdminPassword;

            if (string.IsNullOrEmpty(check.Info.UserName))
            {
                throw new ShtrihApiException("Имя кассира должно быть задано");
            }

            _driver.TableNumber = 2;
            _driver.RowNumber = 30;
            _driver.FieldNumber = 2;
            _driver.ValueOfFieldString = check.Info.UserName;
            _driver.GetFieldStruct();
            _driver.WriteTable();

            _driver.GetECRStatus();

            if (_driver.ECRMode == 3)
            {
                throw new ShtrihApiException("Следует закрыть смену, прежде чем продолжить");
            }

            if (_driver.ECRMode == 8)
            {
                throw new ShtrihApiException("Есть открытый документ. Продолжите печать или аннулируйте его.");
            }

            _driver.CheckType = (int)check.Info.CheckType;

            if (check.Info.ImagePath != null && check.Info.ImageDockType == ImageDockType.Top)
            {
                var drawResult = DrawImage(check);
                if (drawResult.ResultCode != 0)
                    return drawResult;
            }

            _driver.TableNumber = 18;
            _driver.GetTableStruct();
            _driver.FieldNumber = 9;
            _driver.GetFieldStruct();
            var result1 = _driver.ReadTable();
            var address = _driver.ValueOfFieldString;

            _driver.TableNumber = 18;
            _driver.GetTableStruct();
            _driver.FieldNumber = 7;
            _driver.GetFieldStruct();
            var result2 = _driver.ReadTable();
            var company = _driver.ValueOfFieldString;

            _driver.OpenCheck();

            while (string.IsNullOrEmpty(company) == false)
            {
                var str = company.Substring(0, company.Length < MaxStringLength ? company.Length : MaxStringLength);

                _driver.StringForPrinting = str;
                _driver.PrintString();
                company = company.Substring(str.Length);
            }

            while (string.IsNullOrEmpty(address) == false)
            {
                var str = address.Substring(0, address.Length < MaxStringLength ? address.Length : MaxStringLength);

                _driver.StringForPrinting = str;
                _driver.PrintString();
                address = address.Substring(str.Length);
            }

            if (check.Info.Email != null)
            {
                _driver.CustomerEmail = check.Info.Email;
                _driver.FNSendCustomerEmail();
            }

            decimal sumSale = 0;

            for (var index = 0; index < check.Count(); index++)
            {
                var sale = check.ElementAt(index);

                sumSale += AddSaleToCheck(sale, index + 1);
            }

            _driver.StringForPrinting = new string('=', MaxStringLength);

            if (check.Info.PaymentType == PaymentType.Cash)
                _driver.Summ1 = check.Info.CashValue;//наличные
            _driver.Summ2 = check.Info.PaymentType == PaymentType.CreditCard ? sumSale : 0; //карта
            _driver.Summ3 = 0;
            _driver.Summ4 = 0;
            _driver.DiscountOnCheck = check.Info.DiscountOnCheck;
            _driver.CloseCheck();

            var result = new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);

            _driver.Password = AdminPassword;

            return result;
        }

        public DriverResult RepeatLastCheck()
        {
            CheckConnection();

            _driver.RepeatDocument();

            return new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);
        }

        public DriverResult CancelCheck()
        {
            CheckConnection();

            _driver.CancelCheck();

            return new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);
        }

        public DriverResult ContinueLastCheck()
        {
            CheckConnection();

            _driver.ContinuePrint();

            return new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);
        }

        /// <summary>
        /// Проверка связи с устройством
        /// </summary>
        /// <exception cref="Exception">Нет связи</exception>
        public void CheckConnection()
        {
            lock (_driver)
            {
                Thread thread = new Thread(() => _driver.CheckConnection());
                thread.Start();

                Thread.CurrentThread.Join(Timeout * 1000);

                if (thread.ThreadState == ThreadState.Running)
                {
                    thread.Abort();
                    throw new Exception("Нет связи");
                }
            }
        }
        
        private decimal AddSaleToCheck(Sale sale, int index)
        {
            //регистрация операции
            _driver.Quantity = sale.Info.Quantity;
            _driver.Price = sale.Info.Price;

            if (_taxValues.ContainsKey(sale.Info.Tax * 100) == false)
                throw new ShtrihApiException($"В ККМ налога {sale.Info.Tax}% нет. Обратитесь к администратору.");

            _driver.Tax1 = _taxValues[sale.Info.Tax * 100];
            _driver.Tax2 = 0;
            _driver.Tax3 = 0;
            _driver.Tax4 = 0;
            _driver.StringForPrinting = $"//{sale.Info.Name}";
            _driver.FNDiscountOperation();

            var price = sale.Info.Price * (decimal)sale.Info.Quantity;
            double tax = sale.Info.Tax;

            //печать

            var taxValue = Math.Round((double)price / (1 + tax / 100) * tax / 100, 2);

            string pointers = new string('.', MaxStringLength);

            var titleString = $"{index}.Наименование ";

            var firstName = sale.Info.Name.Length > MaxStringLength - titleString.Length
                ? sale.Info.Name.Substring(0, MaxStringLength - titleString.Length)
                : sale.Info.Name;

            var name = sale.Info.Name.Length > MaxStringLength - titleString.Length
                ? sale.Info.Name.Substring(firstName.Length)
                : null;

            List<string> names = new List<string>
            {
                $"{titleString}{firstName}"
            };

            while (string.IsNullOrEmpty(name) == false)
            {
                var str = name.Substring(0, name.Length < MaxStringLength ? name.Length : MaxStringLength);

                names.Add(str);
                name = name.Substring(str.Length);
            }
            
            _driver.CarryStrings = true; //не ясно как работает
            names.ForEach(x =>
            {
                _driver.StringForPrinting = x;
                _driver.PrintString();
            });

            var priceString = $"{sale.Info.Price:N2}";
            _driver.StringForPrinting = $"Цена{pointers.Substring(0, MaxStringLength - 4 - priceString.Length)}{priceString}";
            _driver.PrintString();

            var quantityString = $"{sale.Info.Quantity}";
            _driver.StringForPrinting = $"Количество{pointers.Substring(0, MaxStringLength - 10 - quantityString.Length)}{quantityString}";
            _driver.PrintString();

            var taxString = $"{taxValue:N2}";
            _driver.StringForPrinting = $"НДС {tax}%{pointers.Substring(0, MaxStringLength - 7 - taxString.Length)}{taxString}";
            _driver.PrintString();

            var sumString = $"{price:N2}";
            _driver.StringForPrinting = $"Стоимость{pointers.Substring(0, MaxStringLength - 9 - sumString.Length)}{sumString}";
            _driver.PrintString();

            return price;
        }

        private DriverResult DrawImage(Check check)
        {
            _driver.FileName = check.Info.ImagePath;
            _driver.FirstLineNumber = 1;
            _driver.LastLineNumber = 256;
            _driver.LoadImage();
            _driver.Draw();

            return new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);
        }

        public DriverResult OpenSession()
        {
            CheckConnection();

            _driver.OpenSession();

            return new DriverResult(_driver.ResultCode, _driver.ResultCodeDescription);
        }

        public DriverResult CloseSession()
        {
            return ZReport();
        }
    }
}
