using System;
using System.Windows.Forms;
using ShtrihApi;

namespace ConsoleShtrikhApi
{
    /// <summary>
    /// ������� <see cref="IShtrikhApi"/>
    /// </summary>
    public class ShtrikhApiFactory
    {
        /// <summary>
        /// ����� ���� �� �������
        /// </summary>
        /// <param name="company">�������� �������� ��������</param>
        /// <param name="address">����� �������� ��������</param>
        /// <returns></returns>
        public IShtrikhApi ConsoleApiCreate(string company, string address)
        {
            var api = new ConsoleShtrikhApi(company, address);

            api.CheckPrinted += ApiOnCheckPrinted;

            return api;
        }

        private void ApiOnCheckPrinted(object sender, CheckPrintedEventArgs checkPrintedEventArgs)
        {
            checkPrintedEventArgs.CheckText.ForEach(Console.WriteLine);
            foreach (string s in checkPrintedEventArgs.CheckText)
            {
                MessageBox.Show(s);
            }
        }
    }
}