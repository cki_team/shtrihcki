using System;
using System.Collections.Generic;

namespace ConsoleShtrikhApi
{
    /// <summary>
    /// ��������� ������� ������ ����
    /// </summary>
    public class CheckPrintedEventArgs : EventArgs
    {
        /// <summary>
        /// ����� ����
        /// </summary>
        public List<string> CheckText { get; }

        public CheckPrintedEventArgs(List<string> checkText)
        {
            CheckText = checkText;
        }
    }
}