﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ConsoleShtrikhApi
{
    [ComVisible(true)]
    public class HelloWorld
    {
        private  string _name;


        [ComVisible(true)]
        public string TestMethod(string name)
        {
            _name = name;
            System.Windows.Forms.MessageBox.Show($"Hello world, {_name}");
            return "Result";
        }
    }
}
